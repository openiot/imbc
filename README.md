# Internet of Things Message-based Communication (IMBC)

The IMBC scope is to provide network management for heterogenous IoT systems. IMBC is applied atop the IoT protocols utilised in a heterogenous IoT application which facilitates management features (e.g. device management, message handling, etc.) agnostic to the IoT transport protocol.

IMBC is a dictionary of services uniquely identified by an 8-bit identifier. The services describe specific functions (e.g. temperature measurement, firmware update, etc.) utilised by the central manager to communicate the desired commands to the end-nodes or to process the information send by the end-nodes. IMBP defines services responsible for device configuration, device control, payload identification and error handling.

The messages comprise at least one semantic data service representation which intern consists of two parts: the **service identifier** and the **service message**. The service identifier determines the service for which the following service message is intended. The service message contains the actual data according with the service definition (e.g. data type, data length).  

For detailed documentation read the following paper: [https://www.mdpi.com/1424-8220/20/3/861](https://www.mdpi.com/1424-8220/20/3/861). 