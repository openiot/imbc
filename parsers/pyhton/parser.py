import crcmod
import json
crc16 = crcmod.mkCrcFun(0x11021, 0x0000, False, 0x0000)
ids = json.load(open("../../imbc.json"))

def parse_message(payload=None):
    services = {}

    i = 0
    while i < len(payload):

        id = hex(payload[i])[2:].zfill(2)
        if id not in ids:
            return {}

        service_id, values, length = parse_service(id, ids[id], payload[i:])
        if not service_id:
            return {}

        services[ids[service_id]['service']] = values
        i += length + 1

    return services


def parse_service(service_id, service, payload):
    try:
        if 'length' in service:
            values = []

            array = bytearray(payload)[1:service['length']+1]
            array_value = ''.join([hex(x)[2:].zfill(2) for x in array]).zfill(len(array)*2)

            if len(array_value)/2 != ids[service_id]['length']:
                return None, None, None

            value = bytes_to_service_value(ids[service_id]['type'], array_value)

            length = len(array)

            return service_id, value, length

        elif 'variable-length' in service:
            values = []

            byte_array_length = int.from_bytes(bytearray(payload)[1:service['variable-length']+1], byteorder='big')
            array = bytearray(payload)[1+service['variable-length']:1+service['variable-length']+byte_array_length]
            array_value = ''.join([hex(x)[2:].zfill(2) for x in array]).zfill(len(array)*2)

            if len(array_value)/2 != byte_array_length:
                return None, None, None

            values = bytes_to_service_value(ids[service_id]['type'], array_value)

            length = service['variable-length'] + byte_array_length

            return service_id, values, length

        elif 'list-length' in service:
            values = []

            list_length = service['list-length']

            for j in range(0, list_length):
                element_length = service['element-length']
                element = bytearray(payload)[j*element_length + 1:j*element_length + 1 + element_length]
                element_value = ''.join([hex(x)[2:].zfill(2) for x in element]).zfill(len(element) * 2)

                values.append(bytes_to_service_value(ids[service_id]['type'], element_value))

            length = list_length*service['element-length']

            return service_id, values, length

        elif 'variable-list-length' in service:
            values = []

            array_length = int.from_bytes(bytearray(payload)[1:service['variable-list-length']+1], byteorder='big')
            for j in range(0, array_length):
                element = bytearray(payload)[j+2:j+2+service['element-length']]
                element_value = ''.join([hex(x)[2:].zfill(2) for x in element]).zfill(len(element) * 2)

                values.append(ids[element_value]['service'])

            length = service['variable-list-length'] + array_length*service['element-length']

            return service_id, values, length

        elif 'mask' in service:
            values = []

            service_id = hex(payload[1])[2:].zfill(2)

            if service_id not in ids:
                return None, None, None

            sources = ''.join(format(byte, '08b') for byte in payload[2:2+service['mask']])

            j = 0
            for source in sources:
                if source == '1':
                    service_bytes_value = payload[2+j+service['mask']:2+j+service['mask']+ids[service_id]['length']]
                    service_value = ''.join([hex(x)[2:].zfill(2) for x in service_bytes_value]).zfill(len(service_bytes_value) * 2)

                    if len(service_value)/2 != ids[service_id]['length']:
                        return None, None, None

                    values.append(bytes_to_service_value(ids[service_id]['type'], service_value))
                    j += ids[service_id]['length']
                else:
                    values.append(None)

            length = 1 + service['mask'] + j

            return service_id, values, length

    except Exception as e:
        logger.error(f"Error parsing IMBC payload: {str(e)}")
        return None, None, None


def bytes_to_service_value(value_type, value):
    result = None
    if value_type == 'float':
        result = bytes_to_float(value)
    elif value_type == 'integer':
        result = bytes_to_integer(value)
    elif value_type == 'boolean':
        result = bytes_to_boolean(value)
    elif value_type == 'byte':
        result = bytes_to_list(value)

    return result


def pack_message(payload=None):
    services = apps.get_app_config('imbc').services

    result = bytearray()

    for service, value in payload.items():
        sequence = pack_service(services[service], value)
        result += sequence

    return result


def pack_service(service_id, value):
    services = apps.get_app_config('imbc').services
    service = ids[service_id]

    result = bytearray()

    if 'length' in service:
        if isinstance(value, list) and check_matrix(value) and service['type'] == 'byte':
            result += variable_list_values_to_bytes(service_id, service['type'], value)
        elif isinstance(value, list) and service['type'] == 'byte':
            result += bytes.fromhex(service_id)
            result += service_value_to_bytes(service['type'], value)
        elif isinstance(value, list):
            result += variable_list_values_to_bytes(service_id, service['type'], value)
        elif not isinstance(value, list):
            result += bytes.fromhex(service_id)
            result += service_value_to_bytes(service['type'], value)

    elif 'variable-length' in service:
        if isinstance(value, list) and not check_matrix(value):
            result += bytes.fromhex(service_id)
            result += service_variable_value_to_bytes(service, value)

        elif isinstance(value, list) and check_matrix(value):
            result += array_variable_list_values_to_bytes(service_id, service, value)

    elif 'list-length' in service:
        if isinstance(value, list) and check_matrix(value):
            logger.warning(value)
            result += array_list_values_to_bytes(service_id, service, value)

        elif isinstance(value, list) and not check_bytes_matrix(value):
            result += bytes.fromhex(service_id)
            result += list_values_to_bytes(service_id, service, value)

    elif 'variable-list-length' in service:
        if service['service'] == 'dual_config':
            result += bytes.fromhex(service_id)
            result += bytes([len(value)])
            for val in value:
                result += bytes.fromhex(services[val])

        elif service['service'] == 'uplink_config':
            result += bytes.fromhex(service_id)
            result += bytes([len(value)])
            for val in value:
                result += bytes.fromhex(services[val])

        elif service['service'] == 'downlink_config':
            result += bytes.fromhex(service_id)
            result += bytes([len(value)])
            for val in value:
                result += bytes.fromhex(services[val])

    return result


def service_value_to_bytes(value_type, value):
    result = bytearray()

    if value_type == 'float':
        result = float_to_bytes(value)
    elif value_type == 'integer':
        result = integer_to_bytes(value)
    elif value_type == 'boolean':
        result = boolean_to_bytes(value)
    elif value_type == 'byte':
        result = list_to_bytes(value)

    return result


def service_variable_value_to_bytes(service, value):
    result = bytearray()

    max_length = 2 ^ (service['variable-length'] * 8)
    if len(value) <= max_length:
        result += len(value).to_bytes((len(value).bit_length() + 7) // 8, 'big') or b'\0'
        result += bytes(value)

    return result


def bytes_to_float(value):
    return struct.unpack('>f', bytes.fromhex(value))[0]


def float_to_bytes(value):
    return bytearray(struct.pack(">f", value))


# def bytes_to_short_integer(value):
#     return None
#
#
# def short_integer_to_bytes(value):
#     return None


def bytes_to_integer(value):
    return int.from_bytes(bytes.fromhex(value), byteorder='big')


def integer_to_bytes(value):
    return value.to_bytes(4, 'big')


# def bytes_to_long_integer(value):
#     return None
#
#
# def long_integer_to_bytes(value):
#     return None


def bytes_to_boolean(value):
    result = None
    if bytes.fromhex(value) == b'\x00':
        result = False
    else:
        result = True

    return result


def boolean_to_bytes(value):
    result = bytearray()

    if value == True:
        result += bytearray(b'\xff')
    else:
        result += bytearray(b'\x00')

    return result


def bytes_to_list(value):
    return list(bytes.fromhex(value))


def list_to_bytes(value):
    return bytes(value)


def list_values_to_bytes(service_id, service, values):
    sequence = bytearray()

    for value in values:
        sequence += service_value_to_bytes(service['type'], value)

    return sequence


def array_list_values_to_bytes(service_id, service, values):
    sequence = bytearray()

    if len(values) < 9:
        sequence += bytearray(b'\x0a')
        bits = list('00000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += list_values_to_bytes(service_id, service, values[i])

        sequence += bytes.fromhex(service_id)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    elif len(values) > 8 and len(values) <=16:
        sequence += bytearray(b'\x0b')
        bits = list('0000000000000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += list_values_to_bytes(service_id, service, values[i])

        sequence += bytes.fromhex(service_id)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    return sequence


def variable_list_values_to_bytes(srv, value_type, values):
    sequence = bytearray()

    if len(values) < 9:
        sequence += bytearray(b'\x0a')
        bits = list('00000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += service_value_to_bytes(value_type, values[i])

        sequence += bytes.fromhex(srv)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    elif len(values) > 8 and len(values) <=16:
        sequence += bytearray(b'\x0b')
        bits = list('0000000000000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += service_value_to_bytes(value_type, values[i])

        sequence += bytes.fromhex(srv)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    return sequence


def array_variable_list_values_to_bytes(service_id, service, values):
    sequence = bytearray()

    if len(values) < 9:
        sequence += bytearray(b'\x0a')
        bits = list('00000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += service_variable_value_to_bytes(service, values[i])

        sequence += bytes.fromhex(service_id)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    elif len(values) > 8 and len(values) <=16:
        sequence += bytearray(b'\x0b')
        bits = list('0000000000000000')
        vals = bytearray()

        for i in range(0, len(values)):
            if values[i] is False or values[i]:
                bits[i] = '1'
                vals += service_variable_value_to_bytes(service, values[i])

        sequence += bytes.fromhex(service_id)
        sequence += bitstring_to_bytes(''.join(bits))
        sequence += vals

    return sequence


def bitstring_to_bytes(s):
    return int(s, 2).to_bytes(len(s) // 8, byteorder='big')


def check_matrix(values):
    if isinstance(values, list):
        for value in values:
            if isinstance(value, list):
                return True
    return False


def check_bytes_matrix(values):
    if isinstance(values, list):
        for value in values:
            if isinstance(value, list):
                for val in value:
                    if isinstance(val, list):
                        return True
    return False
